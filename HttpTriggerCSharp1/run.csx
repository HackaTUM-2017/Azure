#r "Microsoft.WindowsAzure.Storage"
#r "Newtonsoft.Json"

using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;
using System.Configuration;
using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types
using Newtonsoft.Json;
using System.Text;



/*
    Returns the following values:
    CHANNELNAME,PROPABILITYFORTHISCHANNEL,CHANNELID
*/
public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, TraceWriter log)
{
    /*
    * 1. Dispatch to logo finder
    * 2. Channel from logo finder
    * 3. access current TV program of the channel
    * 4. request current TV program from Redis
    * 5. if chat in Redis exists -> return chat link
    * 6. otherwise: create a new chat, add it to Redis, return chat link
    */

    // get path for blob from url parameter
    string path = req.GetQueryNameValuePairs()
        .FirstOrDefault(q => string.Compare(q.Key, "path", true) == 0)
        .Value;
    //path = "20171118_002351.jpg";
//path="myimage.jpg";
    // download blob from storage
    //byte[] imagedata = GetImageFromBlob(path).Result;


    // send image to logo finder
    string logoresult = MakePredictionRequest("https://screenimages.blob.core.windows.net/testimages/" + path).Result;

    // parse response
    var json = JsonConvert.DeserializeObject<LogoDetection>(logoresult);
    TagPrediction[] preds = json.Predictions;
    // get tv channel with highest prediction
    TagPrediction highest = preds.OrderByDescending(item => item.Probability).First();
    log.Info("channel with highest probability: " + highest.Tag + " Pred: " + highest.Probability);


    // create a new channel in slack for this tv channel
string body = "{\"name\":\""+highest.Tag+"\"}";
    var httpWebRequest = getRequest("https://slack.com/api/channels.create");
    httpWebRequest.ContentLength = body.Length;
        Stream newStream = httpWebRequest.GetRequestStream ();
        newStream.Write(Encoding.ASCII.GetBytes(body), 0, body.Length);
        HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();




    var httpWebRequest2 = getRequest("https://slack.com/api/channels.list");
        Stream newStream2 = httpWebRequest2.GetRequestStream ();
        newStream2.Write(Encoding.ASCII.GetBytes(body), 0, body.Length);
        HttpWebResponse response2 = (HttpWebResponse)httpWebRequest2.GetResponse();
        // Get the stream associated with the response.
        Stream receiveStream = response2.GetResponseStream ();
        // Pipes the stream to a higher level stream reader with the required encoding format. 
        StreamReader readStream = new StreamReader (receiveStream, Encoding.UTF8);
        string response_string2 = readStream.ReadToEnd ();
        response2.Close ();
        readStream.Close ();
        
        log.Info(response_string2);

        var res = JsonConvert.DeserializeObject<ChannelListResult>(response_string2);
        string channel_name = highest.Tag;
        string channel_id = res.channels.Where(x => x.name == channel_name).First().id;
        string channel_prob = highest.Probability.ToString("F3");
        

    log.Info("" + channel_name + "," + channel_prob + "," + channel_id);
    return req.CreateResponse(HttpStatusCode.OK, "" + channel_name + "," + channel_prob + "," + channel_id);

    /*// Get request body
    dynamic data = await req.Content.ReadAsAsync<object>();

    // Set name to query string or body data
    image = image ?? data?.image;
    
    log.Info(MakePredictionRequest(image).Result);

    return image == null
        ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
        : req.CreateResponse(HttpStatusCode.OK, "Hello " + image);*/
}

public class ChannelResult {
    public string name {get;set;}
    public string id {get;set;}
}
public class ChannelListResult {
    public ChannelResult[] channels {get;set;}
}

static HttpWebRequest getRequest(string url) {
    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Accept = "*/*";
        httpWebRequest.Method = "POST";
        httpWebRequest.Headers.Add("Authorization", "Bearer xoxp-274669223061-275604111127-273870242832-4e928dbc3576d7044dc6ea35e861cf5e");
    return httpWebRequest;
}



static async Task<string> MakePredictionRequest(string image_url)
{
    var client = new HttpClient();

    // Request headers - replace this example key with your valid subscription key.
    client.DefaultRequestHeaders.Add("Prediction-Key", "127468139f664a8fbd07049980724207");

    // Prediction URL - replace this example URL with your valid prediction URL.
    string url = "https://southcentralus.api.cognitive.microsoft.com/customvision/v1.0/Prediction/c7405082-050c-4d3a-96c5-811f1dbc1fe7/url?iterationId=5e540802-ef1c-47e5-907f-99177456630d";

    HttpResponseMessage response;

    // Request body. Try this sample with a locally stored image.
    //byte[] byteData = GetImageAsByteArray(imageFilePath);
    string body = "{\"Url\": \"" + image_url + "\"}";
    using (var content = new StringContent(body))
    {
        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        response = await client.PostAsync(url, content);
        return (await response.Content.ReadAsStringAsync());
    }
}


public class LogoDetection
{
    public string Id { get; set; }
    public string Project {get;set;}
    public string Iteration {get;set;}
    public string Created {get;set;}
    public TagPrediction[] Predictions {get;set;}
}

public class TagPrediction
{
    public string TagId {get; set;}
    public string Tag {get;set;}
    public double Probability{get; set;} 
}


/*
    DEAD CODE: download blob
*/
static async Task<byte[]> GetImageFromBlob(string path) {
    
    // Retrieve storage account from connection string.
    CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
        ConfigurationManager.AppSettings["AzureWebJobsStorage"]);

    // Create the blob client.
    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

    // Retrieve reference to a previously created container.
    CloudBlobContainer container = blobClient.GetContainerReference("testimages");

    // Retrieve reference to a blob named "photo1.jpg".
    CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);

    // Save blob contents to a file.
    MemoryStream ms = new MemoryStream();
    blockBlob.DownloadToStream(ms);
    return ms.ToArray();
}